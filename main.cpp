#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <limits>
#include <iterator>

#include "task.h"

using namespace std;

size_t schrage_rpq(vector<Task> zadania, vector<Task> & zadania_uszeregowane)
{
    vector<Task> zadania_gotowe;
    //vector<Task> zadania_uszeregowane;

    make_heap(zadania.begin(),zadania.end(),more_than_r()); // kopiec min
    size_t t=zadania.front().r;

    while(!zadania.empty() || !zadania_gotowe.empty())
    {
        while(!zadania.empty() && (*zadania.begin()).r<=t)
        {
            zadania_gotowe.push_back(zadania.front());
            push_heap(zadania_gotowe.begin(),zadania_gotowe.end(),less_than_q());
            pop_heap(zadania.begin(),zadania.end(),more_than_r());
            zadania.pop_back();
        }
        if (zadania_gotowe.empty())
            t = zadania.front().r;
        else
        {
            zadania_uszeregowane.push_back(zadania_gotowe.front());
            pop_heap(zadania_gotowe.begin(),zadania_gotowe.end(),less_than_q());
            zadania_gotowe.pop_back();
            t+=zadania_uszeregowane.back().p;
        }
    }
    size_t czas=0;
    size_t czas_q=0;
    for(auto & i : zadania_uszeregowane)
    {
        if(i.r>czas)
            czas=i.r;
        czas+=i.p;
        if ((czas + i.q) > czas_q)
            czas_q=(czas + i.q);
    }
    return czas_q;
}


size_t schrage_rpqm(vector<Task> zadania)
{
    vector<Task> zadania_gotowe;
    Task zadanie_l;
    size_t cmax=0;

    zadanie_l.q=numeric_limits<size_t>::max();

    make_heap(zadania.begin(),zadania.end(),more_than_r()); // kopiec min
    size_t t=zadania.front().r;

    while(!zadania.empty() || !zadania_gotowe.empty())
    {
        while(!zadania.empty() && (*zadania.begin()).r<=t)
        {
            zadania_gotowe.push_back(zadania.front());
            push_heap(zadania_gotowe.begin(),zadania_gotowe.end(),less_than_q());
            Task zadanie_e = zadania.front();
            pop_heap(zadania.begin(),zadania.end(),more_than_r());
            zadania.pop_back();
            if (zadanie_e.q > zadanie_l.q) //5
            {
                zadanie_l.p=t-zadanie_e.r;
                t=zadanie_e.r;
                if (zadanie_l.p>0)
                {
                    zadania_gotowe.push_back(zadanie_l);
                    push_heap(zadania_gotowe.begin(),zadania_gotowe.end(),less_than_q());
                }
            }
        }
        if (zadania_gotowe.empty())
            t = zadania.front().r;
        else
        {
            zadanie_l = zadania_gotowe.front();
            t+=zadanie_l.p;
            pop_heap(zadania_gotowe.begin(),zadania_gotowe.end(),less_than_q());
            zadania_gotowe.pop_back();
            cmax=max(cmax,t+zadanie_l.q);
        }

    }
    return cmax;
}

size_t carlier(vector<Task> zadania, size_t UB)
{
    vector<Task> zadania_schrage; //pi
    vector<Task> zadania_uszeregowane; //pi*
    size_t LB;

    size_t U = schrage_rpq(zadania,zadania_schrage);
    if (U<UB)
    {
        UB=U;
        zadania_uszeregowane=zadania_schrage;
    }

    size_t czas=0;
    size_t czas_q=0;

    size_t a,b,c;
    a=zadania_schrage.size()-1;

    //obliczanie b
    for(auto itr = zadania_schrage.begin(); itr!=zadania_schrage.end(); ++itr)
    {
        if((*itr).r>czas)
            czas=(*itr).r;
        czas+=(*itr).p;
        if ((czas + (*itr).q) > czas_q)
        {
            czas_q=(czas + (*itr).q);
            b = itr - zadania_schrage.begin();
        }
    }
    //obliczanie a ( czas_q = cmax )
    for(auto itr = zadania_schrage.begin(); itr!=zadania_schrage.end(); itr++)
    {
        size_t suma_p=0;
        size_t j = itr-zadania_schrage.begin();
        for(size_t i=j;i<=b;i++)
        {
            suma_p+=zadania_schrage[i].p;
        }
        if(czas_q==((*itr).r + suma_p + zadania_schrage[b].q))
        {
            a=j;
            break;
            //itr=--zadania_schrage.end();
        }

    }
    //obliczanie c
    c = 0;
    size_t i=b;
    while(zadania_schrage[i].q>=zadania_schrage[b].q && i>=a)
    {
        i--;
    }
    if(i<a) //nie znaleziono c
        return czas_q;
    else
        c=i;
//    for(size_t i=b; i>=a; i--)
//        if(zadania_schrage[i].q<zadania_schrage[b].q)
//        {
//            c=i;
//            break;
//        }
//    if(c==0)
//    {
//        return czas_q;
//    }


    size_t r_, p_, q_;

    r_ = (*min_element(zadania_schrage.begin()+c+1,zadania_schrage.begin()+b,less_than_r())).r;
    q_ = (*min_element(zadania_schrage.begin()+c+1,zadania_schrage.begin()+b,less_than_q())).q;
    p_=0;
    //for(auto itr = zadania_schrage.begin() +=c+1; itr!=(zadania_schrage.begin()+=b); ++itr)
    //    p_+=(*itr).p;
    for(i=c+1;i<=b;i++)
        p_+=zadania_schrage[i].p;

    size_t r_pic = zadania_schrage[c].r;
    zadania_schrage[c].r = max(zadania_schrage[c].r, r_ + p_);
    LB = schrage_rpqm(zadania_schrage);
    if (LB < UB)
        return carlier(zadania_schrage,UB);
    zadania_schrage[c].r=r_pic;


    size_t q_pic = zadania_schrage[c].q;
    zadania_schrage[c].q = max(zadania_schrage[c].q, q_ + p_);
    LB = schrage_rpqm(zadania_schrage);
    if (LB < UB)
        return carlier(zadania_schrage,UB);
    zadania_schrage[c].q=q_pic;
    return czas_q;
}



int main()
{
    size_t i, j;
    size_t suma=0;
    ifstream input_file;
    size_t liczba_zadan;
    vector <Task> zadania;
    //vector <Task> zadania_carlier;

    string pliki[3] = {"in200.txt","in100.txt","in50.txt"};

    for(j=0;j<3;j++)
    {
        zadania.clear();
        input_file.open(pliki[j].c_str(), ios::out);
        if (input_file.is_open())
        {
            input_file >> liczba_zadan;
            input_file.ignore(80,'\n');
            zadania.resize(liczba_zadan);

            for (i=0;i<liczba_zadan;i++)
            {
                zadania[i].id=i;
                input_file >> zadania[i].r;
                input_file >> zadania[i].p;
                input_file >> zadania[i].q;
            }
            input_file.close();
        }
        else
            cout << "Błąd otwierania pliku";

        size_t czas = 0;
        czas = carlier(zadania,(size_t)numeric_limits<size_t>::max);
        //zadania_carlier = carlier(zadania,(size_t)numeric_limits<size_t>::max);

        cout << "Carlier, " << zadania.size() << " zadan, czas: " << czas << endl;
        suma+=czas;
    }
    cout << endl << "Suma Carlier: " << suma << endl;
    return 0;
}
